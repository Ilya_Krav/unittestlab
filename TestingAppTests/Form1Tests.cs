﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestingApp.Tests
{
    [TestClass]
    public class Form1Tests
    {
        private const string _path = @"Logins/";

        [TestMethod]
        public void Loging_KnownLoginAndKnownPassword_false()
        {
            var knownLogin = "ilyaKrav";
            var knownPassword = "1233";
            var expected = false;

            var loginSystem = new Form1();
            var methodReturn = loginSystem.Loging(knownLogin, knownPassword);

            Assert.AreEqual(expected, methodReturn);
        }

        [TestMethod]
        public void Loging_UnknownLoginAndUnknownPassword_false()
        {
            var unknownLogin = "unknownLogin";
            var unknownPassword = "unknownPassword";
            var expected = false;

            var loginSystem = new Form1();
            var methodReturn = loginSystem.Loging(unknownLogin, unknownPassword);

            Assert.AreEqual(expected, methodReturn);
        }

        [TestMethod]
        public void CheckData_EmptyLoginAndEmptyPassword_false()
        {
            var emptyLogin = "";
            var emptyPassword = "";
            var expected = false;

            var loginSystem = new Form1();
            var methodReturn = loginSystem.CheckData(emptyLogin, emptyPassword);

            Assert.AreEqual(expected, methodReturn);
        }

        [TestMethod]
        public void Loging_KnownLoginAndUnknownPassword_false()
        {
            var knownLogin = "ilyaKrav";
            var unknownPassword = "unknownPassword";
            var expected = false;

            var loginSystem = new Form1();
            var methodReturn = loginSystem.Loging(knownLogin, unknownPassword);

            Assert.AreEqual(expected, methodReturn);
        }

        [TestMethod]
        public void Loging_UnknownLoginAndknownPassword_false()
        {
            var unknownLogin = "unknownLogin";
            var knownPassword = "1233";
            var expected = false;

            var loginSystem = new Form1();
            var methodReturn = loginSystem.Loging(unknownLogin, knownPassword);

            Assert.AreEqual(expected, methodReturn);
        }

        [TestMethod]
        public void CheckData_BigLengthLogin_false()
        {
            var knownLogin = "abcabcabcabcabcabc"; // > 15
            var knownPassword = "1233";
            var expected = false;

            var loginSystem = new Form1();
            var methodReturn = loginSystem.Loging(knownLogin, knownPassword);

            Assert.AreEqual(expected, methodReturn);
        }

        [TestMethod]
        public void Register_LoginWithSpace_false()
        {
            var login = "abc abc";
            var password = "1233";
            var expected = false;

            var loginSystem = new Form1();
            var methodReturn = loginSystem.Register(login, password);

            var path = _path + login + ".txt";
            File.Delete(path);

            Assert.AreEqual(expected, methodReturn);
        }

        [TestMethod]
        public void Register_ExistLogin_true()
        {
            var login = "ilyaKrav";
            var password = "1233";
            var expected = true;

            var loginSystem = new Form1();
            var methodReturn = loginSystem.Register(login, password);

            Assert.AreEqual(expected, methodReturn);
        }

        [TestMethod]
        public void password_PasswordChar_IsNotNull()
        {
            var loginSystem = new Form1();
            var returned = loginSystem.password.PasswordChar;

            Assert.IsNotNull(returned);
        }

        [TestMethod]
        public void login_MaxLength_15()
        {
            var expected = 15;

            var loginSystem = new Form1();
            var returned = loginSystem.login.MaxLength;

            Assert.AreEqual(expected, returned);
        }

    }
}