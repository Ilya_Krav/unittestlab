﻿using System;
using System.IO;
using System.Windows.Forms;

namespace TestingApp
{
    public partial class Form1 : Form
    {
        private const string _path = @"Logins\";
        private bool _reg;

        public Form1()
        {
            if (!Directory.Exists(_path))
            {
                Directory.CreateDirectory(_path);
            }

            InitializeComponent();
        }

        public bool Register(string log, string pass)
        {
            var fullName = _path + log + ".txt";

            DirectoryInfo directoryInfo = new DirectoryInfo(_path);
            var loginsInfo = directoryInfo.GetFiles();

            foreach (var fileInfo in loginsInfo)
            {
                if (fileInfo.Name == log + ".txt")
                {
                    return true;
                }
            }

            File.WriteAllText(fullName, pass);
            enter.Text = "Вход";
            return false;
        }

        public void enter_Click(object sender, EventArgs e)
        {
            var loginText = login.Text;
            var passwordText = password.Text;

            if (CheckData(loginText, passwordText))
            {
                if (_reg)
                {
                    _reg = Register(loginText, passwordText);
                    return;
                }

                loginText += ".txt";

                Loging(loginText, passwordText);
            }
        }

        public bool CheckData(string log, string pass)
        {
            if (log == "")
            {
                //MessageBox.Show("Не введён логин!");
                return false;
            }

            if (log.Length > 15)
            {
                return false;
            }

            if (pass == "")
            {
                //MessageBox.Show("Не введён пароль!");
                return false;
            }

            return true;
        }

        public bool Loging(string log, string pass)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(_path);
            var loginsInfo = directoryInfo.GetFiles();

            foreach (var loginFile in loginsInfo)
            {
                if (log.ToLower() == loginFile.Name.ToLower())
                {
                    var passwordFileStream = loginFile.OpenRead();
                    var passwordFile = new StreamReader(passwordFileStream).ReadToEnd();

                    if (pass == passwordFile)
                    {
                        exit.Visible = true;
                        return true;
                    }
                }
            }

            //MessageBox.Show("Неверный логин или пароль!");

            return false;
        }

        public void exit_Click(object sender, EventArgs e)
        {
            exit.Visible = false;
        }

        private void register_Click(object sender, EventArgs e)
        {
            enter.Text = "Зарегистрироваться";
            _reg = true;
        }
    }
}
